from multidimensional_urlencode import urlencode    # PHP-style url encoder
from urllib.parse import urlunparse
import json
import requests


class Mobizon(object):
    def __init__(self, api_key, scheme=None):
        with open('config.json') as file:
            self.config = json.load(file)
        self.api_url = self.config['apiURL']
        self.scheme = scheme or self.config['scheme']
        self.version = self.config['version']
        self.api_key = api_key

    def _join_url(self, path, params):
        return urlunparse([
            self.scheme,
            self.api_url,
            path,
            False,
            params,
            False
        ])

    def _parse_params(self, params):
        if not params:
            params = {}
        params['apiKey'] = self.api_key
        return urlencode(params)

    def _call_method(self, path, params=None):
        request = requests.get(self._join_url(path, self._parse_params(params)))
        if request.status_code == 200:
            return json.loads(request.text)
        else:
            return {'error': request.status_code}

    def create_short_link(self, link):
        params = {'data': {'fullLink': link, 'status': 1}}
        path = 'service/Link/Create/'
        return self._call_method(path, params)

    def send_message(self, recipient, text, signature=None, **kwargs):
        additional_params = {'name', 'deferredToTs', 'mclass', 'validity'}
        additional_params = kwargs.keys() & set(additional_params)
        params = {'recipient': recipient, 'text': text}
        if signature:
            params['signature'] = signature
        if additional_params:
            params['params'] = {}
            for i in additional_params:
                params['params'][i] = kwargs[i]

        path = 'service/Message/SendSMSMessage/'
        return self._call_method(path, params)

    def get_sms_list(self):
        path = 'service/Message/List/'
        request = self._call_method(path)

        request = {'code': 0, 'data': [{
            'id': i['id'],
            'status': i['status'],
            'text': i['text'],
            'signature': i['from'],
            'to': i['to'],
            'startSendTs': i['startSendTs'],
            'statusUpdateTs': i['statusUpdateTs'],
            'groups': i['groups']
        } for i in request['data']['items']]}
        return request

    def get_sms_status(self, *args):
        path = 'service/Message/GetSMSStatus/'
        params = {'ids': args}
        return self._call_method(path, params)

    def get_balance(self):
        path = 'service/User/GetOwnBalance/'
        return self._call_method(path)
